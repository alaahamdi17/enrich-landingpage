// ========================= navbar ==================

// const navSlide = () => {
//   const burger = document.querySelector(".burger");
//   const nav = document.querySelector(".nav-links");
//   const navLinks = document.querySelectorAll(".nav-links li");

//   //Toggle Nav
//   burger.addEventListener("click", () => {
//     nav.classList.toggle("nav-active");

//     //Animate Links
//     navLinks.forEach((link, index) => {
//       if (link.style.animation) {
//         link.style.animation = "";
//       } else {
//         link.style.animation = `navLinkFade 0.5s ease forwards ${
//           index / 7 + 0.5
//         }s`;
//       }
//     });

//     //burger animation
//     burger.classList.toggle("toggle");
//   });
// };

// navSlide();

// ========================= phone number ==================
$(function () {
  $("input[type='tel']").on("input", function (e) {
    $(this).val(
      $(this)
        .val()
        .replace(/[^\+0-9]/g, "")
    );
  });
});

//====================================swiper-team========================

var swiper1 = new Swiper(".swiper-team", {
  slidesPerView: 3,
  spaceBetween: 10,
  loop: false,
  loopFillGroupWithBlank: true,

  breakpoints: {
    1920: {
      slidesPerView: 5,
    },
    992: {
      slidesPerView: 3,
    },
    320: {
      slidesPerView: 1,
    },
  },
  navigation: {
    nextEl: ".swiper-team .swiper-button-next",
    prevEl: ".swiper-team .swiper-button-prev",
  },
});

//======================swiper-initialize says========================
var swsays = new Swiper(".swiper-initialize", {
  slidesPerView: 5,
  spaceBetween: 30,
  slideToClickedSlide: true,

  breakpoints: {
    1920: {
      slidesPerView: 5,
    },
    992: {
      slidesPerView: 3,
    },
    768: {
      slidesPerView: 2,
    },
    320: {
      slidesPerView: 1,
    },
  },
  navigation: {
    nextEl: ".says-sec .title .swiper-button-next",
    prevEl: ".says-sec .title .swiper-button-prev",
  },
});

//================================swiper curious===========================
const swiper3 = new Swiper(".swiper-cursious", {
  // Optional parameters
  direction: "horizontal",
  loop: true,
  autoHeight: false,
  centeredSlides: true,
  slidesPerView: 1,
  // Responsive breakpoints
  slidesPerView: "auto",
  slideToClickedSlide: true,

  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 40,
    },
    992: {
      slidesPerView: 1.6,
      spaceBetween: 50,
    },
  },

  // Navigation arrows
  navigation: {
    nextEl: ".swiper-cursious .swiper-button-next",
    prevEl: ".swiper-cursious .swiper-button-prev",
  },
});

//==============================general-swiper============================
var swiper4 = new Swiper(".general-swiper", {
  slidesPerView: 4,
  spaceBetween: 28,
  loop: false,
  loopFillGroupWithBlank: true,

  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },

  breakpoints: {
    280: {
      slidesPerView: 1,
      spaceBetween: 20,
    },

    500: {
      slidesPerView: 1.5,
      spaceBetween: 25,
    },
    640: {
      slidesPerView: 1.8,
      spaceBetween: 27,
    },

    768: {
      slidesPerView: 2.5,
      spaceBetween: 30,
    },

    992: {
      slidesPerView: 3.2,
      spaceBetween: 30,
    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 25,
    },
    1300: {
      slidesPerView: 4,
      spaceBetween: 23,
    },
  },

  navigation: {
    nextEl: ".general-sec .title .swiper-button-next",
    prevEl: ".general-sec .title .swiper-button-prev",
  },
});

//validate form
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  "use strict";

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll(".needs-validation");

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach(function (form) {
    form.addEventListener(
      "submit",
      function (event) {
        if (!form.checkValidity()) {
          event.preventDefault();
          event.stopPropagation();
        }

        form.classList.add("was-validated");
      },
      false
    );
  });
})();

//pop up
// window.open(
//   "https://www.example.com",
//   "my-popup",
//   "width=500,height=300,left=0,top=0"
// );
// function openPopup() {
//   document.getElementById("my-popup").style.display = "block";
// }
//  function openPopup() {
//    window.open("my-popup.html", "my-popup", "width=500,height=500");
//  }
//
// function closePopup() {
//   document.getElementById("my-popup").style.display = "none";
// }

let popup = document.getElementById("popUp");
function openPopup() {
  popup.classList.add("open-popup");
}
function closePopup() {
  console.log("close popup");
  popup.classList.remove("open-popup");
}

// animation cruds

// document.getElementById("original").addEventListener("click", function () {
//   document.getElementById("new").classList.add("animate");
// });

//animation

// console.log(original)
// original.addEventListener("click", function () {
//   newDiv.classList.add("animate")
//   console.log(this.animate)
// })

// document.getElementById("original").addEventListener("click", function () {
//   document.getElementById("new").classList.toggle("animate");
// });
// document.getElementById("new").addEventListener("animationend", function () {
//   // Do something when the animation ends
// });

// function show() {
//   // newDiv.classList.add("animate");
//   console.log("uhiujhnik");
//   // document.getElementById("original").addEventListener("click", function () {
//   //   document.getElementById("new").classList.add("animate");
//   // });

//  const style=  original.style.color = "red"

// original.addEventListener("mouseover", function () {
//   // newDiv.classList.add("animate");
//  newDiv.style.color = "red";

// });
// original.addEventListener("mousemove", function () {
//   newDiv.classList.remove("animate");
// });
const newDiv = document.getElementsByClassName("second-div");
const feature = document.getElementsByClassName("feature");
const crud = document.getElementsByClassName("crud");

const widthBtn = document.getElementsByClassName("widthBtn");

const ourServiceCard = document.querySelectorAll(
  ".general-swiper .swiper-wrapper .swiper-slide"
);
ourServiceCard.forEach((item, i) => {
  item.addEventListener("mouseenter", () => {
    newDiv[i].classList.add("animate");
    newDiv[i].classList.add("animate__fadeIn");
    crud[i].classList.add("crudd");
    feature[i].classList.add("hide");
    widthBtn[i].classList.remove("buttons");
    widthBtn[i].classList.add("chooseee");
  });
  item.addEventListener("mouseleave", () => {
    newDiv[i].classList.remove("animate");
    crud[i].classList.remove("crudd");
    feature[i].classList.remove("hide");
    widthBtn[i].classList.remove("chooseee");
  });
});
